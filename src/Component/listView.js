import React from 'react'
import firebase from 'firebase'
import _ from 'lodash';
import Genlistview from './GenListview'

class listView extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          StoreData: []
        };  
      }
      componentDidMount() {
        let app = firebase.database().ref('Product');
        app.on('value', snapshot => {
          this.getData(snapshot.val());
          //console.log(snapshot.val())
        });
      }
      getData(values) {
        let data = values;
        let StoreData = _(data)
          .keys()
          .map(productKey => {
            let cloned = _.clone(data[productKey]);
            cloned.key = productKey;
            return cloned;
          }).value();
        this.setState({
          StoreData: StoreData
        });
      }
      render() {
        let Conntent = this.state.StoreData.map((info) => {
          return (
            <div className={this.props.col} ><Genlistview pdName={info.Pd_Name} pdImgUrl={info.Pd_imgurl} pdPrice={info.Pd_Price} /></div>        
          )
        });
        return (
          <div>
            <div className="card-deck">
            {Conntent}
            </div>
          </div>
        );
      }
}
export default listView