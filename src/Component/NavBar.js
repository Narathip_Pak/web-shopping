import React, { Component } from 'react'
import firebase from 'firebase'

const auth = firebase.auth
const provider = new firebase.auth.FacebookAuthProvider();

class Navbar extends Component {
  state = {
    user: null
  }

  login = () => {
    auth().signInWithPopup(provider)
      .then(({ user }) => {
        this.setState({ user })
      })
  }

  logout = () => {
    auth().signOut().then(() => {
      this.setState({ user: null })
    })
  }

  CheckLogin() {
    return (<button onClick={this.login}>Login with Facebook</button>)
  }

  render() {
    const { user } = this.state
    console.log(user)
    if(user){
      localStorage.setItem('LoginParam', "1");
    } else {
      localStorage.setItem('LoginParam', "0");
    }
    return (
      <nav className="navbar navbar-light bg-light">
        <a className="navbar-brand" href="/">
          Web Shopping online store
        </a>
        <img src="images/img.jpg" alt="" />{user ? `Hi, ${user.displayName}!` : this.CheckLogin()}
      </nav>
    )
  }
}
export default Navbar