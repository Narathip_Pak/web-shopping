import React, { Component } from 'react'
import firebase from 'firebase'

class Genlistview extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imgUrl: "Loading",
        }
    }
    checkButton = () => {
        if(localStorage.getItem('LoginParam') === "1"){
            return (<button className="btn btn-success float-right" >Buy</button>)
        } else {
            return (<button className="btn btn-success float-right" disabled >Buy</button>)
        }
        
      }
    render() {
        const imageRef = firebase.storage().ref().child(`${this.props.pdImgUrl}`)
        const sampleImage = imageRef.getDownloadURL().then(result => {
            this.setState({
                imgUrl: result,
            })
        });
        //console.log(this.state.imgUrl)
        return (
            <div className="card">
                <img className="card-img-top" src={this.state.imgUrl} alt="Loading" />
                <div className="card-body">
                    <h5 className="card-title">{this.props.pdName}</h5>
                    <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    <hr />
                    <div>
                    <span className="float-left"><p>Price : {this.props.pdPrice}$</p></span>
                    <span className="float-right"><input value="1" style={{width:"50px"}} type="number" ></input></span>
                    </div>
                    
                </div>
                <div className="card-footer">

                    <small className="text-muted">Last updated 3 mins ago</small>
                    {this.checkButton()}
                </div>
            </div>

        )
    }
    
}
export default Genlistview