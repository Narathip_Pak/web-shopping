import React from 'react'
import { NavLink } from 'react-router-dom'

class SideBar extends React.Component {
    render() {
        return (
            <div>
          <nav>
              <ul className="remove-bullet">
                <li className="setLi" ><NavLink exact to="/" activeClassName="active" className="navbar-item">Home</NavLink></li>           
                <li className="setLi" ><NavLink to="/Cats" activeClassName="active" className="navbar-item">Cats</NavLink></li>
                <li className="setLi" ><NavLink to="/Dogs" activeClassName="active" className="navbar-item">Dogs</NavLink></li>
                </ul>
        </nav> 
      </div>
        )
    }
}
export default SideBar