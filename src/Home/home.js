import React from 'react';
import Left from './Left'
import Right from './Right'
import Top from './top'

class Home extends React.Component{
    render(){
        return(
            <div className="row">
                <Top />
                <Left/>
                <Right/>    
            </div>
        )

    }
}
export default Home