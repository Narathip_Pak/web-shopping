import React from 'react';
import { Route } from 'react-router-dom'
import Cats from '../Page/Cats'
import Dogs from '../Page/Dogs'

class Right extends React.Component{
    render(){
return(
      <div className="col-md-10">
        <Route exact path="/" />
        <Route path="/Cats" component={Cats} />
        <Route path="/Dogs" component={Dogs} />
      </div>
)
    }
}
export default Right