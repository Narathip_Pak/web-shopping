import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Home from './Home/home'
import firebase from 'firebase'

class App extends Component {
  constructor(props) {
    super(props); var config = {
      apiKey: "AIzaSyAv-rVsioIUoXJjfszr14JVTR2THf_lfRo",
      authDomain: "reactfirebase-35016.firebaseapp.com",
      databaseURL: "https://reactfirebase-35016.firebaseio.com",
      projectId: "reactfirebase-35016",
      storageBucket: "reactfirebase-35016.appspot.com",
      messagingSenderId: "1026235246105"
    };
    firebase.initializeApp(config);
  }

  render() {
    return (
      <Home />
    );
  }
}

export default App;
